package com.example.k00na_.slidenerd_blogreader;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity {

    PlaceholderFragment taskFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null){
            taskFragment = new PlaceholderFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(taskFragment, "TaskFragment")
                    .commit();
        } else {
            taskFragment = (PlaceholderFragment)getSupportFragmentManager().findFragmentByTag("TaskFragment");
        }

        taskFragment.startTask();

    }

    /**/
    public static class PlaceholderFragment extends Fragment{

        TechCrunchTask downloadTask;
        public PlaceholderFragment(){
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            setRetainInstance(true);
        }

        public void startTask(){
            if(downloadTask != null){
                downloadTask.cancel(true);
            } else {
                downloadTask = new TechCrunchTask();
                downloadTask.execute();
            }
        } // haha

    }

    public static class TechCrunchTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {
            String downloadURL = "http://feeds.feedburner.com/techcrunch/android?format=xml";
            try {
                URL url = new URL(downloadURL);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setRequestMethod("GET");
                InputStream inputStream = connection.getInputStream();
                processXML(inputStream);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }

            return null;
        }

        public void processXML(InputStream inputStream) throws ParserConfigurationException, IOException, SAXException {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document xmlDocument = documentBuilder.parse(inputStream); /* <-- builds the DOM tree */
            Element rootElement = xmlDocument.getDocumentElement(); // document element is the root element
            Log.i("rootElement", rootElement.getTagName());
        }
    }
}
